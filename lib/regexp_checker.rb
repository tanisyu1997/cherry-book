print 'Test:? '
text = gets.chomp

begin
  print 'Pattern:? '
  pattern = gets.chomp
  regexp = Regexp.new(pattern)
rescue RegexpError => e
  puts "Invalid pattern: #{e.message}"
  retry
end

maches = text.scan(regexp)

if maches.size > 0
  puts "mached: #{maches.join(', ')}"
else
  puts 'Nothing mached'
end