def greeting
  puts 'おはよう'

  text = yield 'こんにちは','Hello'

  puts text
  puts 'こんばんは'
end

greeting do |a, b|
  a + b
end