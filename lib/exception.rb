puts 'Start'

module Greeter
  def hello
    'hello'    
  end  
end

retry_count = 0

begin
  1 / 0
  greeter = Greeter.new
rescue NoMethodError
  puts 'NoMethodErrorです。'
rescue NameError
  puts 'NameErrorです。'
rescue StandardError => e
  puts 'その他のエラーです。'
  puts "エラークラス; #{e.class}"
  puts "エラーメッセージ; #{e.message}"
  puts "backtrace ----"
  puts "#{e.backtrace}"
  puts "----end"
  puts '例外が発生したが、このまま続行する'

  retry_count += 1
 if retry_count <= 2
  retry
 else
  puts 'retryに失敗しました。'
 end
end

puts 'End'