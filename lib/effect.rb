module Effects
  def self.reverse
    ->(words) do
      words.split(' ').map(&:reverse).join(' ')
    end
  end

  def self.echo(count)
    ->(words) do
      words.chars.map {|char|
        char*count unless char == ' '
      }.join('')
    end
  end

  def self.loud(count)
    ->(words) do
      words.split(' ').map(&:upcase!).join(' ') << '!'*count
    end
  end
end