require'../lib/deep_freeze'

class Bank
  extend DeepFreezable

  CURRENCIES = deep_freeze({'Japan' => 'yen', 'US' => 'dollars', 'india' => 'rupee'})
end
