require 'minitest/autorun'

class SampleTest < Minitest::Test
  def test_sample_success
    assert_equal 'RUBY', 'ruby'.upcase
  end

  def test_sample_failure
    refute 'RUBY', 'ruby'.capitalize
  end
end