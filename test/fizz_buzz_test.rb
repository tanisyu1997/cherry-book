require 'minitest/autorun'
require '../lib/fizz_buzz.rb'

class FizzBuzzTest < Minitest::Test
  def test_fizz_buzz_success
    assert_equal '1', fizz_buzz(1)
    assert_equal '2', fizz_buzz(2)
    assert_equal 'Fizz', fizz_buzz(3)
    assert_equal 'Buzz', fizz_buzz(5)
    assert_equal 'Fizz Buzz', fizz_buzz(15)
    
  end

  def test_fizz_buzz_failure
    refute fizz_buzz(3) == '3', fizz_buzz(3)
    refute fizz_buzz(5) == '5', fizz_buzz(5)
    refute fizz_buzz(15) == '15', fizz_buzz(15)
    refute fizz_buzz(15) == 'Fizz', fizz_buzz(15)
    refute fizz_buzz(15) == 'Buzz', fizz_buzz(15)
  end
end