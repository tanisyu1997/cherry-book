require 'minitest/autorun'
require '../lib/word_synth.rb'
require '../lib/effect.rb'

class WordSynthTest < Minitest::Test
  def test_play_without_effect
    synth = WordSynth.new
    assert 'Ruby Is Fun!', synth.play('Ruby Is Fun!')
  end

  def test_play_with_effects
    synth = WordSynth.new
    synth.add_effect(Effects.reverse)
    assert 'ybuR sI !nuF', synth.play('Ruby Is Fun!')

    synth.add_effect(Effects.echo(1))
    assert 'yybbuuRR ssII !!nnuuFF', synth.play('Ruby Is Fun!')

    synth.add_effect(Effects.loud(2))
    assert 'YYBBUURR SSII !!NNUUFF!!', synth.play('Ruby Is Fun!')
  end
end