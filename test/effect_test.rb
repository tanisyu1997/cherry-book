require 'minitest/autorun'
require '../lib/effect.rb'

class EffectTest < Minitest::Test
  def test_reverse
    effect = Effects.reverse
    assert 'ybuR sI !nuF', effect.call('Ruby Is Fun!')
  end

  def test_echo
    effect = Effects.echo(1)
    assert 'RRuubbyy IIss FFuunn!!', effect.call('Ruby Is Fun!')

    effect = Effects.echo(2)
    assert 'RRRuuubbbyyy IIIsss FFFuuunnn!!!', effect.call('Ruby Is Fun!')
  end

  def test_loud
    effect = Effects.loud(1)
    assert 'RUBY IS FUN!!', effect.call('Ruby Is Fun!')

    effect = Effects.loud(2)
    assert 'RUBY IS FUN!!!!', effect.call('Ruby Is Fun!')
  end
end